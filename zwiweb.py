#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Goal: Make a ZWI file from any webpage.
# S.Chekanov (KSF)
#
# Changelog:
# @version 1.1,    Mar 18, 2024
#    - new options -c, -d -o
# @version 1.0,    Mar 10, 2024
#    - added HTML simplification
#    - specify license 
#
# @version 1.0beta, Feb 24, 2024 
#   First version

import os,zlib,zipfile,glob,sys
import time, string
import random
import html2text
import re,json
from time import time 
import shutil

def install(package):
    print("pip install --user "+package) 
    sys.exit()

try:
    import html2text 
    #print("module 'html2text' is installed")
except ModuleNotFoundError:
    print("module 'html2text' is not installed")
    install("html2text") # the install function from the question

try:
    import bs4 
    #print("module 'bs4' is installed")
except ModuleNotFoundError:
    print("module 'bs4' is not installed")
    install("bs4") # the install function from the question

def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))

from hashlib import sha1
from os import path
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import argparse

# import all modules from this directory
script_dir=os.path.dirname(os.path.realpath(__file__))
sys.path.append(script_dir)
sys.path.append(script_dir+"/src/")
sys.path.append(script_dir+"/pywebcopy/")
from pywebcopy import save_webpage
from short_description import *
from hash_article import *
from lxml.html.clean import Cleaner

kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument("-i", '--input', help="Input URL of some webpage")
parser.add_argument('-v', '--verbose', action='store_true', help="show verbose output")
parser.add_argument('-s', '--simplify', action='store_true', help="simplify HTML file by removing CSS and JS")
parser.add_argument('-l', '--license', help="specify a license of the website. Default is CC BY-SA 3.0")
parser.add_argument('-c', '--comment', help="specify a comment for metadata")
parser.add_argument('-d', '--directory', help="directory for output ZWI file")
parser.add_argument('-o', '--output', help="output ZWI file name (directory can be included)")
parser.add_argument('-p', '--publisher', action='store_true', help="include publisher to the ZWI file name")

args = parser.parse_args()

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

mdebug=False
if (args.verbose): mdebug=True;

mpub=False
if (args.publisher): mpub=True;

msimplify=False
if (args.simplify): msimplify=True;

project_folder="/tmp/zwiweb"
url=args.input.strip()
print("URL =",args.input)
print("debug = ",mdebug)
print("simplify HTML = ",msimplify)
xlicense="CC BY-SA 3.0"
if (args.license != None):
    if (len(args.license)>1):
      xlicense=args.license
      print("License = ",xlicense)

xcomment=None
if (args.comment != None):
    if (len(args.comment)>0):
      xcomment=args.comment
      print("Comment = ",xcomment)


xdirectory=None 
if (args.directory != None):
    if (len(args.directory)>0):
      xdirectory=args.directory
      print("Output directory = ",xdirectory)

xoutput=None
if (args.output != None):
    if (len(args.output)>0):
      xoutput=args.output
      print("Output ZWI file = ",xoutput)

asource=urlparse(url).netloc;

# random project name
project_name=id_generator()

# clear 1h old 
if (os.path.exists( project_folder  ) ): 
  ago = time() - (3600)
  for i in os.listdir(project_folder):
    fpath = os.path.join(project_folder, i)
    if os.stat(fpath).st_mtime < ago:
        if os.path.isfile(fpath):
            try:
                os.remove(fpath)
            except:
                print("Could not remove file:", i)
        else:
            try:
                shutil.rmtree(fpath)
            except:
                print("Could not remove directory:", i)


xpath=urlparse(url).path
#xname=os.path.basename(xpath)
#print(xpath,xname)
if (xpath.startswith('/')):
       xpath=xpath[1:] 

HTMLfile=xpath.rsplit('.', maxsplit=1)[0]
ZWIname=HTMLfile.replace("/","#")+".zwi"
HTMLfile=HTMLfile.rsplit('.', maxsplit=1)[0]
HTMLfile=HTMLfile.split("/")[-1]

makeTXT=True

# internal path
internal_path=project_folder+'/'+project_name+'/'
if os.path.isdir(internal_path):
                          shutil.rmtree(internal_path)
                          print("Clear "+internal_path)

save_webpage(
      url,
      project_folder,
      project_name,
      bypass_robots=True,
      debug=mdebug,
      open_in_browser=False,
      delay=None,
      threaded=False,
      )

#################### do not change #############################:)

# Create sha1 of a string
def make_sha1(s, encoding='utf-8'):
    return sha1(s.encode(encoding)).hexdigest()

def clean_html(dirty_html):
   cleaner = Cleaner(page_structure=False,
                  meta=False,
                  embedded=True,
                  links=True,
                  style=True,
                  processing_instructions=False,
                  inline_style=True,
                  scripts=True,
                  javascript=True,
                  comments=True,
                  frames=True,
                  forms=True,
                  annoying_tags=True,
                  remove_unknown_tags=True,
                  safe_attrs_only=True,
                  safe_attrs=frozenset(['src', 'href', 'title', 'h1', 'h2', 'h3', 'name', 'id', 'li', 'ol', 'ul']),
                  remove_tags=('span', 'font', 'div')
                  )
   html = cleaner.clean_html(dirty_html)
   # multiple empty lines
   lines = ['']
   for line in html.split("\n"):
            line=line.replace("\r"," ")
            line=line.replace("\t"," ")
            line = re.sub(' +', ' ', line) 
            line=line.strip();
            if len(line)<1: continue
            lines.append(line+"\n")
   html = "".join(lines)
   return html

# Get file sha1 hash
def fileSha1(xfile):
  BLOCKSIZE = 65536
  hasher = sha1()
  if (path.exists(xfile)):
    with open(xfile, 'rb') as afile:
       buf = afile.read(BLOCKSIZE)
       while len(buf) > 0:
          hasher.update(buf)
          buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()
  return ""

# make TXT file
def getTXT(html):
    parser = html2text.HTML2Text()
    parser.ignore_links = True
    parser.ignore_images = True
    parser.body_width = 0
    parser.ignore_emphasis = True
    parser.wrap_links = True
    articletxt = parser.handle(html)
    xlines = articletxt.splitlines();
    non_empty_lines="";
    for line in xlines:
        line=line.strip()
        if len(line)<3: line="\n"; 
        if line=="|": line=" ";
        line=line.replace("---","\n")
        non_empty_lines += line + "\n"
        # print(len(line)," ",line)
    articletxt=non_empty_lines
    articletxt=re.sub(r'\n\s*\n', '\n\n', articletxt) # replace 2 new lines 
    articletxt=articletxt.strip()
    if (len(articletxt)<300):
          print("Article has the length =",len(articletxt)," ->  too short. Exit!")
          return ""
    if (len(articletxt)<500):
        if (articletxt.find("no text in this page")>-1):
          return ""
    if (len(articletxt)<500):
        if (articletxt.find("redirect"))>-1: 
          return ""
    return articletxt


def makeZWI():
    global ZWIname 
    if (mdebug): print("HTML SEARCH PATTERN:",HTMLfile)
    html_files=glob.glob(internal_path+'**',recursive=True)
    HTML_FILE,HTML_FILE2,HTML_FILE3="","",""
    for fi in html_files:
        if (msimplify):
          notTake=False
          if (fi.find(".css")>-1):  notTake=True;
          if (fi.find(".js")>-1):   notTake=True;
          if (fi.find(".mjs")>-1):  notTake=True;
          if (fi.find(".xml")>-1):  notTake=True;
          if (fi.find(".woff")>-1): notTake=True;
          if (notTake):
                    os.remove(fi)
                    continue
        if (fi.find(HTMLfile)>-1 and fi.find(".htm")>-1): 
          HTML_FILE=fi
        if (fi.find("index.htm")>-1): HTML_FILE2=fi 
        if (fi.find(".htm")>-1):      HTML_FILE3=fi 

    if (len(HTML_FILE)<2):
              HTML_FILE=HTML_FILE2 # maybe index? 
              if (len(HTML_FILE)<2):
                  HTML_FILE=HTML_FILE3 # maybe index?
                  if (len(HTML_FILE)<2):
                     print("Cannot find the HTML file from ",url)
                     sys.exit()
    if (mdebug): print("Main HTML=",HTML_FILE)

    dirname = os.path.dirname(HTML_FILE)
    filename = os.path.basename(HTML_FILE)
    print("Root directory=",dirname)

    # fetch all directories and files 
    style_directories=os.listdir( dirname+"/" )

    # create data directory
    datadir=dirname+"/data";
    if os.path.isdir(datadir) == False:
        if (mdebug): print("Create:"+datadir)
        os.mkdir(datadir)

    # move all directories to data directory
    media=[]
    for k in style_directories:
        if (k.find(filename)>-1): continue 
        if (mdebug): print(k)
        media.append(k+"/")
        source=dirname+"/"+k
        destination=dirname+"/data/"+k
        dest = shutil.move(source, destination)  
        #print(dest)

    # make replacements of styles..
    with open(HTML_FILE, 'r') as file:
           htmldata = file.read()

    for med in media:
            htmldata=htmldata.replace(med,"data/"+med)

    # overwite the OLD html
    file1 = open(HTML_FILE, "w")  # write mode
    file1.write(htmldata)
    file1.close()


    xmedia={}
    assets={}
    html_files=glob.glob(internal_path+'**',recursive=True)
    for fi in html_files:
      if (fi == HTML_FILE): continue
      if os.path.isfile(fi): 
         xdir=fi.replace(dirname+'/',"")
         xdir=xdir.replace(internal_path,"")
         xmedia[xdir]=fileSha1( fi  ) 
         assets[xdir]=fi

    ARTICLE_FILE=dirname+"/article.html"
    print('base=',HTML_FILE)
    # rename article
    os.rename(HTML_FILE,ARTICLE_FILE)
    with open(ARTICLE_FILE) as f:
         htmlnew = f.read()

    ZWI_VERSION=1.33 # Version of ZWI files
    # ISO Language Codes https://www.w3schools.com/tags/ref_language_codes.asp
    Lang="en"

    # using the BeautifulSoup module to get title 
    atitle=""
    soup = BeautifulSoup(htmlnew, 'html.parser')
    for title in soup.find_all('title'):
          atitle=title.get_text()
          break

    # simplify if needed
    if (msimplify):
                  htmlnew = clean_html(htmlnew);

    stime=str(int(time()));
    ncategories=[]
    generator="ZWIWeb"
    comment="";
    if (msimplify): 
              comment="simplified HTML ";
    if (xcomment != None):
                  comment=comment+xcomment;
    topics=[]
    revisions=[]
    rating=[]
    primary="article.html";
    # sha1
    content={}
    content["article.html"]=make_sha1(htmlnew)

    if (mpub == True ):
                      ZWIname = asource+"#"+ZWIname
    if (xoutput != None):
                      ZWIname=xoutput
    if (xdirectory != None):
                      ZWIname = xdirectory+os.sep+ZWIname

    z = zipfile.ZipFile(ZWIname, 'w', compression=zipfile.ZIP_DEFLATED)  # this is a zip archive
    z.writestr("article.html", htmlnew)
    articletxt=""
    if (makeTXT):
       articletxt = getTXT( htmlnew )
       z.writestr("article.txt", articletxt)
       content["article.txt"]=make_sha1(articletxt);
    description=getShortDescription(asource,"",htmlnew,articletxt);


    metadata = {
            "ZWIversion":ZWI_VERSION,
            "Title":atitle,
            "Lang":Lang,
            "Content":content,
            "Primary":primary,
            "Revisions":revisions,
            "Publisher":asource,
            "CreatorNames":[],
            "ContributorNames":[],
            "LastModified":stime,
            "TimeCreated":stime,
            "Categories":ncategories,
            "Topics":topics,
            "GeneratorName":generator,
            "Rating":rating,
            "Description":description,
            "Comment":comment,
            "License":xlicense,
            "SourceURL":url
            }

    z.writestr("metadata.json", json.dumps(metadata,ensure_ascii=False,indent=4))
    z.writestr("media.json", json.dumps(xmedia,indent=4,ensure_ascii=False,sort_keys=True))
    for key, value in assets.items():
        #print(key, ":", value)
        z.write(value,key)
    z.close()
    print("Created=",ZWIname)



if __name__ == '__main__':
# Execute when the module is not initialized from an import statement.
  makeZWI()

