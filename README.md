# ZWIWeb

ZWIWeb python project is used to create a ZWI file from any webpage.

## How to Install

You need several things: A Linux OS (Ubuntu, for example). 
The Linux server should have Python3 installed. For example, for Ubuntu servers, run these commands: 

```
sudo apt install python3
git clone https://gitlab.com/ks_found/zwiweb.git
cd zwiweb 
git clone https://github.com/rajatomar788/pywebcopy.git
```

How to test: 

```
python3 zwiweb.py -i https://handwiki.org/blog/how-to-write-a-book-using-google-docs
```

This example creates a ZWI file with all HTML resources and styles. 

The size of the full ZWI file can be quite large. One can reduce its size by simplifying the HTML file:

```
python3 zwiweb.py -s  -i http://plainshumanities.unl.edu/encyclopedia/doc/egp.ind.002
```

In the default setting, this removes all JS and CSS files.
If you would like to assign a license for the website, use option "l":

```
python3 zwiweb.py -s -l "CC BY-SA 3.0" -i https://jwork.org/home/can-a-human-brain-hold-your-life-experience 
```

By default, the domain name is included in ZWI file metadata, but not in the file name. To include the domain name to the file name, use
the option "-p". 
Check all available options as:

```
python3 zwiweb.py -h
```


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Licensed under the Apache License, Version 2.0 (the "License")
http://www.apache.org/licenses/LICENSE-2.0


## Project status
In progress.

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
