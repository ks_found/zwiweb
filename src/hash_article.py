import hashlib
import base64
#
# Create a short, fairly unique, urlsafe hash for the input string.
# https://roytanck.com/2021/10/17/generating-short-hashes-in-php/
def generate_id( input_str, length = 12 ):
   hash_base64 =  (base64.b64encode(hashlib.sha256(input_str.encode('utf-8')).digest()))
   hash_base64=hash_base64.decode('ascii')
   # // Replace non-urlsafe chars to make the string urlsafe.
   hash_urlsafe  = hash_base64.replace('+', '0');
   hash_urlsafe  = hash_urlsafe.replace('/', 'a');
   hash_urlsafe  = hash_urlsafe.replace('-', 'b');
   hash_urlsafe  = hash_urlsafe.replace('_', 'c');
   # do not start has from -. Replace with 0
   hash_urlsafe=hash_urlsafe.strip()
   #if (hash_urlsafe.startswith('-')):
   #                              hash_urlsafe=hash_urlsafe.replace('-', '0', 1)
   return hash_urlsafe[0:length]

#id=generate_id("sandbox/sandbox.org/glacial-man-in-ohio1");
#print(id)

